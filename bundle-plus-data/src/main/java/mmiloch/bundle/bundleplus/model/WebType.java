package mmiloch.bundle.bundleplus.model;

public class WebType {

    private String webName;

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }
}

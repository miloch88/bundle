package mmiloch.bundle.bundleplus.model;

import javax.xml.crypto.Data;

public class Game {

    private String gameName;
    private Data soldDate;
    private Double soldCost;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public Data getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(Data soldDate) {
        this.soldDate = soldDate;
    }

    public Double getSoldCost() {
        return soldCost;
    }

    public void setSoldCost(Double soldCost) {
        this.soldCost = soldCost;
    }
}

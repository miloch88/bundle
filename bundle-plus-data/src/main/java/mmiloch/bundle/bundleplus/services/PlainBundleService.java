package mmiloch.bundle.bundleplus.services;

import mmiloch.bundle.bundleplus.model.PlainBundle;

import java.util.Set;

public interface PlainBundleService {

    PlainBundle findByName(String last);
    PlainBundle findById(Long id);
    PlainBundle save(PlainBundle plainBundle);
    Set<PlainBundle> findlAll();

}

package mmiloch.bundle.bundleplus.services;

import mmiloch.bundle.bundleplus.model.Game;

import java.util.Set;

public interface GameService {

    Game findByName(String last);
    Game findById(Long id);
    Game save(Game game);
    Set<Game> findlAll();

}

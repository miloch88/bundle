package mmiloch.bundle.bundleplus.services;

import mmiloch.bundle.bundleplus.model.AnnualBundle;

import java.util.Set;

public interface AnnualBundleService {

    AnnualBundle findByName(String last);
    AnnualBundle findById(Long id);
    AnnualBundle save(AnnualBundle annualBundle);
    Set<AnnualBundle> findlAll();

}

package mmiloch.bundle.bundleplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BundlePlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(BundlePlusApplication.class, args);
    }

}
